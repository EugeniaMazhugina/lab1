﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoteList
{
    public class Note
    {
        public string Surname;
        public string Name;
        public string Fathername;
        public string Number;
        public string Country;
        public string DateOfBirth;
        public string Organisation;
        public string Job;
        public string Notes;
        public Note(string Surname, string Name, string Fathername, string Number, string Country, string DateOfBirth, string Organisation, string Job, string Notes)
        {
            this.Surname = Surname;
            this.Name = Name;
            this.Fathername = Fathername;
            this.Number = Number;
            this.Country = Country;
            this.DateOfBirth = DateOfBirth;
            this.Organisation = Organisation;
            this.Job = Job;
            this.Notes = Notes;
        }
    }
}
