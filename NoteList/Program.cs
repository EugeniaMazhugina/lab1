﻿using NoteList;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp
{
    class Notebook
    {
        public static Note[] NoteList = new Note[500];
        public static int Counter = 0;
        static void Main()
        {
            Console.WriteLine("Создание новой записи - 1");
            Console.WriteLine("Редактирование созданных записей - 2");
            Console.WriteLine("Удаление созданных записей - 3");
            Console.WriteLine("Просмотр созданных записей - 4");
            Console.WriteLine("Краткий просмотр записей - 5");
            Console.WriteLine("");

            string variant = Console.ReadLine();
            if (variant == "1")
            {
                CreateNewNote();
                Counter++;
            }
            if (variant == "2")
            {
                EditNote();
            }
            if (variant == "3")
            {
                DeleteNote();
            }
            if (variant == "4")
            {
                ShowNote();
            }
            if (variant == "5")
            {
                ShowAllNotes();
            }
            Console.WriteLine("");
            Main();
            Console.ReadLine();
        }

        public static void CreateNewNote()
        {
            Console.WriteLine(" ");
            Console.Write("Фамилия: ");
            string Surname = Console.ReadLine();
            while (Surname.Length < 1)
            {
                Console.Write("Введите фамилию: ");
                Surname = Console.ReadLine();
            }

            Console.Write("Имя: ");
            string Name = Console.ReadLine();
            while (Name.Length < 1)
            {
                Console.Write("Введите имя: ");
                Name = Console.ReadLine();
            }

            Console.Write("Отчество (поле не является обязательным): ");
            string Fathername = Console.ReadLine();

            Console.Write("Номер телефона: ");
            string Number = Console.ReadLine();
            while (Number.Length < 1)
            {
                Console.Write("Укажите номер телефона: ");
                Number = Console.ReadLine();
            }

            Console.Write("Страна: ");
            string Country = Console.ReadLine();
            while (Country.Length < 1)
            {
                Console.Write("Укажите страну: ");
                Country = Console.ReadLine();
            }

            Console.Write("Дату рождения (поле не является обязательным): ");
            string DateOfBirth = Console.ReadLine();
            Console.Write("Организация (поле не является обязательным): ");
            string Organisation = Console.ReadLine();
            Console.Write("Должность (поле не является обязательным): ");
            string Job = Console.ReadLine();
            Console.Write("Заметки (поле не является обязательным): ");
            string Notes = Console.ReadLine();
            NoteList[Counter] = new Note(Surname, Name, Fathername, Number, Country, DateOfBirth, Organisation, Job, Notes);
            Console.WriteLine(" ");
        }
        public static void EditNote()
        {
            if (Counter == 0)
            {
                Console.WriteLine(" ");
                Console.WriteLine("У вас пока нет контактов");
                Console.WriteLine("");
                Main();
            }
            Console.WriteLine("Выберите номер контакта, который вы бы хотели изменить: ");
            for (int i = 0; i < Counter; i++)
            {
                Console.WriteLine(i + ". " + NoteList[i].Surname);
            }
            Console.WriteLine("");
            try
            {
                int num = Convert.ToInt32(Console.ReadLine());
                if ((num >= Counter) || (num < 0))
                {
                    Console.WriteLine("Некорректное число");
                    Console.WriteLine("");
                    EditNote();
                }
                Console.WriteLine(" ");
                Console.WriteLine("Выберите номер позиции, которую вы бы хотели изменить: ");
                Console.WriteLine("0. главное меню");
                Console.WriteLine("1. фамилия");
                Console.WriteLine("2. имя");
                Console.WriteLine("3. отчество");
                Console.WriteLine("4. номер");
                Console.WriteLine("5. страна");
                Console.WriteLine("6. дата рождения");
                Console.WriteLine("7. организация");
                Console.WriteLine("8. должность");
                Console.WriteLine("9. Заметки");
                Console.WriteLine("");
                char pos = Convert.ToChar(Console.ReadLine());
                switch (pos)
                {
                    case '0':
                        Console.WriteLine("");
                        Main();
                        break;
                    case '1':
                        Console.WriteLine("Введите новую фамилию: ");
                        NoteList[num].Surname = Console.ReadLine();
                        while (NoteList[num].Surname.Length < 1)
                        {
                            Console.WriteLine("Введите новую фамилию: ");
                            NoteList[num].Surname = Console.ReadLine();
                        }
                        break;

                    case '2':
                        Console.WriteLine("Введите новое имя: ");
                        NoteList[num].Name = Console.ReadLine();
                        while (NoteList[num].Name.Length < 1)
                        {
                            Console.WriteLine("Введите новое имя: ");
                            NoteList[num].Name = Console.ReadLine();
                        }
                        break;

                    case '3':
                        Console.WriteLine("Введите новое отчество: ");
                        NoteList[num].Fathername = Console.ReadLine();
                        break;
                    case '4':
                        Console.WriteLine("Введите новый номер: ");
                        NoteList[num].Number = Console.ReadLine();
                        while (NoteList[num].Number.Length < 1)
                        {
                            Console.WriteLine("Введите новый номер: ");
                            NoteList[num].Number = Console.ReadLine();
                        }
                        break;

                    case '5':
                        Console.WriteLine("Введите новое название страны: ");
                        NoteList[num].Country = Console.ReadLine();
                        break;

                    case '6':
                        Console.WriteLine("Введите новую дату рождения: ");
                        NoteList[num].DateOfBirth = Console.ReadLine();
                        break;

                    case '7':
                        Console.WriteLine("Введите новое название организации: ");
                        NoteList[num].Organisation = Console.ReadLine();
                        break;

                    case '8':
                        Console.WriteLine("Введите новую должность: ");
                        NoteList[num].Job = Console.ReadLine();
                        break;

                    case '9':
                        Console.WriteLine("Введите новую заметку: ");
                        NoteList[num].Notes = Console.ReadLine();
                        break;
                    default:
                        Console.WriteLine("Некорректное число");
                        Console.WriteLine("");
                        EditNote();
                        break;
                }

            }
            catch
            {
                Console.WriteLine("Некорректное число");
                Console.WriteLine("");
                EditNote();
            }

        }
        public static void DeleteNote()
        {
            if (Counter == 0)
            {
                Console.WriteLine(" ");
                Console.WriteLine("У вас пока нет контактов");
                Console.WriteLine("");
                Main();
            }
            Console.WriteLine("Выберите номер контакта, который вы бы хотели удалить: ");
            for (int i = 0; i < Counter; i++)
            {
                Console.WriteLine(i + ". " + NoteList[i].Surname);
            }
            Console.WriteLine("");
            try
            {
                int num = Convert.ToInt32(Console.ReadLine());
                if ((num >= Counter) || (num < 0))
                {
                    Console.WriteLine("Некорректное число");
                    Console.WriteLine("");
                    Main();
                }
                while (num < Counter)
                {
                    NoteList[num] = NoteList[num + 1];
                    num++;
                }
                Counter--;
            }
            catch
            {
                Console.WriteLine("Введите только цифру");
                DeleteNote();
            }

        }
        public static void ShowNote()
        {
            if (Counter == 0)
            {
                Console.WriteLine(" ");
                Console.WriteLine("У вас пока нет контактов");
                Console.WriteLine("");
                Main();
            }
            Console.WriteLine("");
            for (int i = 0; i < Counter; i++)
            {
                Console.WriteLine("Фамилия: " + NoteList[i].Surname);
                Console.WriteLine("Имя: " + NoteList[i].Name);
                Console.WriteLine("Отчество: " + NoteList[i].Fathername);
                Console.WriteLine("Номер телефона: " + NoteList[i].Number);
                Console.WriteLine("Страна: " + NoteList[i].Country);
                Console.WriteLine("Дата рождения: " + NoteList[i].DateOfBirth);
                Console.WriteLine("Организация: " + NoteList[i].Organisation);
                Console.WriteLine("Должность: " + NoteList[i].Job);
                Console.WriteLine("Заметки: " + NoteList[i].Notes);
                Console.WriteLine("");
            }
            Console.WriteLine("");
        }
        public static void ShowAllNotes()
        {
            if (Counter == 0)
            {
                Console.WriteLine(" ");
                Console.WriteLine("У вас пока нет контактов");
                Console.WriteLine("");
                Main();
            }
            Console.WriteLine("");
            for (int i = 0; i < Counter; i++)
            {
                Console.WriteLine("Фамилия: " + NoteList[i].Surname);
                Console.WriteLine("Имя: " + NoteList[i].Name);
                Console.WriteLine("Номер телефона: " + NoteList[i].Number);
                Console.WriteLine("");
            }
            Console.WriteLine("");
        }
    }
}
